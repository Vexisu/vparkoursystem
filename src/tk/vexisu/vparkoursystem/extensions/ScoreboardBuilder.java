package tk.vexisu.vparkoursystem.extensions;

import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.AbstractMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Maciek on 2016-08-14.
 */
public class ScoreboardBuilder {

    private Scoreboard scoreboard;
    private String title;
    private Map<String, Integer> scores;
    private List<Team> teams;

    public ScoreboardBuilder(final String title) {
        this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        this.title = title;
        this.scores = Maps.newLinkedHashMap();
        this.teams = Lists.newArrayList();
    }

    public void blankLine() {
        this.add(" ");
    }

    public void add(final String text) {
        this.add(text, null);
    }

    public void add(String text, final Integer score) {
        Preconditions.checkArgument(text.length() < 48, (Object)"text cannot be over 48 characters in length");
        text = this.fixDuplicates(text);
        this.scores.put(text, score);
    }

    private String fixDuplicates(String text) {
        while (this.scores.containsKey(text)) {
            text = String.valueOf(text) + "§r";
        }
        if (text.length() > 48) {
            text = text.substring(0, 47);
        }
        return text;
    }

    private Map.Entry<Team, String> createTeam(final String text) {
        String result = "";
        if (text.length() <= 16) {
            return new AbstractMap.SimpleEntry<Team, String>(null, text);
        }
        final Team team = this.scoreboard.registerNewTeam("text-" + this.scoreboard.getTeams().size());
        final Iterator<String> iterator = Splitter.fixedLength(16).split((CharSequence)text).iterator();
        team.setPrefix((String)iterator.next());
        result = iterator.next();
        if (text.length() > 32) {
            team.setSuffix((String)iterator.next());
        }
        this.teams.add(team);
        return new AbstractMap.SimpleEntry<Team, String>(team, result);
    }

    @SuppressWarnings("deprecation")
    public void build() {
        final Objective obj = this.scoreboard.registerNewObjective((this.title.length() > 16) ? this.title.substring(0, 15) : this.title, "dummy");
        obj.setDisplayName(this.title);
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
        int index = this.scores.size();
        for (final Map.Entry<String, Integer> text : this.scores.entrySet()) {
            final Integer score = (text.getValue() != null) ? text.getValue() : index;
            obj.getScore(text.getKey()).setScore(score);
            --index;
        }
    }

    public void reset() {
        this.title = null;
        this.scores.clear();
        for (final Team t : this.teams) {
            t.unregister();
        }
        this.teams.clear();
    }

    public Scoreboard getScoreboard() {
        return this.scoreboard;
    }

    public static void destroy(final Player... players) {
        for (final Player p : players) {
            final ScoreboardBuilder pk = new ScoreboardBuilder("a");
            pk.build();
            pk.send(p);
        }
    }

    public void send(final Player... players) {
        for (final Player p : players) {
            p.setScoreboard(this.scoreboard);
        }
    }
}
