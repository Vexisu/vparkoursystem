package tk.vexisu.vparkoursystem;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import tk.vexisu.vparkoursystem.commands.JoinCommand;
import tk.vexisu.vparkoursystem.commands.LobbyCommand;
import tk.vexisu.vparkoursystem.commands.LvlCommand;
import tk.vexisu.vparkoursystem.commands.ParkourCommand;
import tk.vexisu.vparkoursystem.listeners.*;
import tk.vexisu.vparkoursystem.utils.ConfigUtil;

/**
 * Created by Maciek on 2016-08-14.
 */
public class Main extends JavaPlugin {

    private static Main instance;

    @Override
    public void onEnable() {
        instance = this;
        saveDefaultConfig();
        getCommands();
        registerEvents();
        ConfigUtil.loadSettingsFromConfig();
    }

    @Override
    public void onDisable() {

    }

    // Get commands and register events

    private void getCommands() {
        getCommand("parkour").setExecutor(new ParkourCommand());
        getCommand("join").setExecutor(new JoinCommand());
        getCommand("lobby").setExecutor(new LobbyCommand());
        getCommand("lvl").setExecutor(new LvlCommand());
    }

    private void registerEvents() {
        Bukkit.getPluginManager().registerEvents(new JoinListener(), this);
        Bukkit.getPluginManager().registerEvents(new MoveListener(), this);
        Bukkit.getPluginManager().registerEvents(new ChatListener(), this);
        Bukkit.getPluginManager().registerEvents(new QuitListener(), this);
        Bukkit.getPluginManager().registerEvents(new KickListener(), this);
    }

    // Getters and setters

    public static Main getInstance() {
        return instance;
    }
}
