package tk.vexisu.vparkoursystem.utils;

import tk.vexisu.vparkoursystem.objects.Parkour;

import java.util.ArrayList;

/**
 * Created by Maciek on 2016-08-14.
 */
public class ParkourUtil {

    private static ArrayList<Parkour> parkours = new ArrayList<>();

    public static ArrayList<Parkour> getParkours() {
        return parkours;
    }

    public static Parkour getParkourByID(int id) {
        for(Parkour p : parkours) {
            if(p.getId() == id) {
                return p;
            }
        }
        return null;
    }
}
