package tk.vexisu.vparkoursystem.utils;

import org.bukkit.Location;
import tk.vexisu.vparkoursystem.Main;
import tk.vexisu.vparkoursystem.objects.FailBlock;
import tk.vexisu.vparkoursystem.objects.Parkour;
import tk.vexisu.vparkoursystem.objects.Score;
import tk.vexisu.vparkoursystem.objects.User;

import java.util.ArrayList;

/**
 * Created by Maciek on 2016-08-15.
 */
public class ConfigUtil {

    private static int multipler;
    private static Location lobby;

    public static void loadSettingsFromConfig(){
        multipler = Main.getInstance().getConfig().getInt("Multipler");
        if(Main.getInstance().getConfig().getString("Lobby") != null){
            lobby = Utils.serializeLocation(Main.getInstance().getConfig().getString("Lobby"));
        }

        System.out.println("[VParkourSystem] Loaded plugin settings...");

        int index = 0;
        if(Main.getInstance().getConfig().getConfigurationSection("Users") != null){
            for(String arg : Main.getInstance().getConfig().getConfigurationSection("Users").getKeys(false)){
                int exp = Main.getInstance().getConfig().getInt("Users." + arg + ".Exp");
                int lvl = Main.getInstance().getConfig().getInt("Users." + arg + ".Level");
                UserUtil.getUsers().add(new User(arg, exp, lvl));
                index++;
            }
        }

        System.out.println("[VParkourSystem] Loaded " + index + " users...");
        index = 0;
        int scoresindex = 0;

        if(Main.getInstance().getConfig().getConfigurationSection("Parkours") !=null){
            for(String arg : Main.getInstance().getConfig().getConfigurationSection("Parkours").getKeys(false)){
                int id = Integer.parseInt(arg);
                int exp = Main.getInstance().getConfig().getInt("Parkours." + arg + ".Exp");
                String name = Main.getInstance().getConfig().getString("Parkours." + arg + ".Name");
                int requiredlvl = Main.getInstance().getConfig().getInt("Parkours." + arg + ".RequiredLvl");
                Location spawn = Utils.serializeLocation(Main.getInstance().getConfig().getString("Parkours." + arg + ".PkSpawn"));
                String failBlocks = Main.getInstance().getConfig().getString("Parkours." + arg + ".FailBlocks");
                String[] failBlocksArray = failBlocks.split(",");
                ArrayList<FailBlock> failBlocksList = new ArrayList<>();
                for (String failBlock : failBlocksArray){
                    String[] failBlockMeta = failBlock.split(":");
                    if(failBlockMeta.length == 1){
                        failBlocksList.add(new FailBlock(Integer.parseInt(failBlockMeta[0]), (byte) 0));
                    }
                    if(failBlockMeta.length == 2){
                        failBlocksList.add(new FailBlock(Integer.parseInt(failBlockMeta[0]), Byte.parseByte(failBlockMeta[1])));
                    }
                }
                ArrayList<Score> scoresList = new ArrayList<>();
                for(String score : Main.getInstance().getConfig().getConfigurationSection("Parkours." + arg + ".Scores").getKeys(false)){
                    scoresList.add(new Score(score, Main.getInstance().getConfig().getLong("Parkours." + arg + ".Scores." + score)));
                    scoresindex++;
                }
                ParkourUtil.getParkours().add(new Parkour(id, name, spawn, exp, requiredlvl, scoresList, failBlocksList));
                index++;
            }
        }

        System.out.println("[VParkourSystem] Loaded " + index + " parkours...");
        System.out.println("[VParkourSystem] Loaded " + scoresindex + " scores...");
    }

    public static int getMultipler() {
        return multipler;
    }

    public static Location getLobby() {
        return lobby;
    }

    public static void setLobby(Location lobby) {
        ConfigUtil.lobby = lobby;
    }
}

