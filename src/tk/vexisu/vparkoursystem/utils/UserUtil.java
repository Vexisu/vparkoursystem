package tk.vexisu.vparkoursystem.utils;

import tk.vexisu.vparkoursystem.objects.User;

import java.util.ArrayList;

/**
 * Created by Maciek on 2016-08-14.
 */
public class UserUtil {

    private static ArrayList<User> users = new ArrayList<>();

    public static ArrayList<User> getUsers() {
        return users;
    }

    public static User getUserByNick(String nick) {
        for(User u : users) {
            if(u.getNick().equals(nick)) {
                return u;
            }
        }
        return null;
    }
}
