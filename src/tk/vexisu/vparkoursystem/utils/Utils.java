package tk.vexisu.vparkoursystem.utils;

import org.bukkit.*;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import tk.vexisu.vparkoursystem.Main;
import tk.vexisu.vparkoursystem.objects.User;

/**
 * Created by Maciek on 2016-08-14.
 */
public class Utils {

    public static String getColor(String string) {
        return ChatColor.translateAlternateColorCodes('&', string);
    }

    public static void sendMessage(CommandSender sender, String message) {
        sender.sendMessage(getColor("&9 > &7" + message));
    }

    public static Location serializeLocation(final String location) {
        final String[] deserializedLocation = location.split(";");
        return new Location(Bukkit.getWorld(deserializedLocation[0]), (double) Double.valueOf(deserializedLocation[1]), (double) Double.valueOf(deserializedLocation[2]), (double) Double.valueOf(deserializedLocation[3]), (float) Float.valueOf(deserializedLocation[4]), (float) Float.valueOf(deserializedLocation[5]));
    }

    public static String deserializeLocation(final Location location) {
        return String.valueOf(location.getWorld().getName()) + ";" + (location.getBlockX() + 0.5) + ";" + location.getBlockY() + ";" + (location.getBlockZ() + 0.5) + ";" + location.getYaw() + ";" + location.getPitch();
    }

    public static void updateParkour(Player player) {
        player.getPlayer().setFoodLevel(20);
        player.getPlayer().setHealth(20);
        player.getPlayer().setExp(0.0f);
        player.getPlayer().setLevel(0);
        player.getPlayer().setFlying(false);
        player.getPlayer().setGameMode(GameMode.ADVENTURE);
        player.getPlayer().setAllowFlight(false);
        player.getPlayer().setWalkSpeed(0.2f);
        player.getPlayer().setFlySpeed(0.1f);
        for(PotionEffect e : player.getActivePotionEffects()){
            player.removePotionEffect(e.getType());
        }
    }

    public static void levelUp(Player p1, User u1) {
        while (u1.getNeedExp() < 1) {
            int level = u1.getLvl() + 1;
            u1.setLvl(level);
            Main.getInstance().getConfig().set("Users." + u1.getNick() + ".Level", level);
            Main.getInstance().saveConfig();
            p1.sendMessage("");
            p1.sendMessage(Utils.getColor("       &9>&7  Awansowales na " + level + " poziom!  &9<"));
            p1.sendMessage("");
            p1.playSound(p1.getLocation(), Sound.LEVEL_UP, 1, 1);
        }
    }
    public static void silentLvlUp(User u1) {
        while (u1.getNeedExp() < 1) {
            int level = u1.getLvl() + 1;
            u1.setLvl(level);
            Main.getInstance().getConfig().set("Users." + u1.getNick() + ".Level", level);
            Main.getInstance().saveConfig();
        }
    }

}
