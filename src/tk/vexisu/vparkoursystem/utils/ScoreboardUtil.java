package tk.vexisu.vparkoursystem.utils;

import org.bukkit.entity.Player;
import tk.vexisu.vparkoursystem.extensions.ScoreboardBuilder;
import tk.vexisu.vparkoursystem.objects.Parkour;
import tk.vexisu.vparkoursystem.objects.Score;

import java.util.*;

/**
 * Created by Maciek on 2016-08-19.
 */
public class ScoreboardUtil {

    private static Comparator<Score> TIMES_COMPARATOR = new Comparator<Score>() {

        public int compare(Score arg0, Score arg1) {
            return (int) (arg0.getTimeInMilis() - arg1.getTimeInMilis());
        }

    };

    public static Collection<Score> sortTimes(Collection<Score> unsortedTimes) {
        List<Score> sortedTimes = new ArrayList<Score>(unsortedTimes);
        Collections.sort(sortedTimes, TIMES_COMPARATOR);
        return sortedTimes;
    }

    public static void sendScoreBoard(Player p1, Parkour pk1){
        ScoreboardBuilder sb = new ScoreboardBuilder(Utils.getColor("&9") + pk1.getName());
        Collection<Score> scores = sortTimes(pk1.getScores());
        int i = 0;
        int x = -1;
        for (Score score : scores) {
            if (i > 9) {
                break;
            }
            sb.add(String.valueOf(Utils.getColor("&7") + score.getTime() + Utils.getColor("&9 > &r&7") + score.getNick()), x);
            i++;
            --x;
        }
        sb.build();
        sb.send(p1);
    }
}
