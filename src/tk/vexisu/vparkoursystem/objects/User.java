package tk.vexisu.vparkoursystem.objects;

import tk.vexisu.vparkoursystem.utils.ConfigUtil;

/**
 * Created by Maciek on 2016-08-14.
 */
public class User {

    private String nick;
    private int exp;
    private int lvl;
    private Timer timer;
    private Parkour activeparkour;

    public User(String nick, int exp, int lvl) {
        this.nick = nick;
        this.exp = exp;
        this.lvl = lvl;
        this.timer = null;
        this.activeparkour = null;
    }

    public int getNeedExp() {
        int level = this.lvl;
        int xp = exp;
        int multipler = ConfigUtil.getMultipler();
        int i = 1;
        int need = 0;
        int needexp = 0;
        while (i <= level){
            need = needexp;
            needexp = (i * multipler) + need ;
            i++;
        }
        return needexp - xp;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    public Timer getTimer() {
        return timer;
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }

    public Parkour getActiveparkour() {
        return activeparkour;
    }

    public void setActiveparkour(Parkour activeparkour) {
        this.activeparkour = activeparkour;
    }
}
