package tk.vexisu.vparkoursystem.objects;

import org.bukkit.Location;
import tk.vexisu.vparkoursystem.Main;
import tk.vexisu.vparkoursystem.utils.ParkourUtil;

import java.util.ArrayList;

/**
 * Created by Maciek on 2016-08-14.
 */
public class Parkour {

    private int id;
    private String name;
    private Location spawn;
    private int exp;
    private int requiredlvl;
    private ArrayList<Score> scores;
    private ArrayList<FailBlock> failblocks;

    public Parkour(int id, String name, Location spawn, int exp, int requiredlvl, ArrayList<Score> scores, ArrayList<FailBlock> failblocks) {
        this.id = id;
        this.name = name;
        this.spawn = spawn;
        this.exp = exp;
        this.requiredlvl = requiredlvl;
        this.scores = scores;
        this.failblocks = failblocks;
    }

    public Score getScoreByNick(String nick){
        for(Score s1 : scores){
            if(s1.getNick().equals(nick)){
                return s1;
            }
        }
        return null;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getSpawn() {
        return spawn;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getRequiredlvl() {
        return requiredlvl;
    }

    public void setRequiredlvl(int requiredlvl) {
        this.requiredlvl = requiredlvl;
    }

    public ArrayList<Score> getScores() {
        return scores;
    }

    public ArrayList<FailBlock> getFailblocks() {
        return failblocks;
    }

    public void setFailblocks(ArrayList<FailBlock> failblocks) {
        this.failblocks = failblocks;
    }

    public void removeParkour(){
        Main.getInstance().getConfig().set("Parkours." + this.id, null);
        Main.getInstance().saveConfig();
        ParkourUtil.getParkours().remove(this);
    }

    public void setSpawn(Location spawn) {
        this.spawn = spawn;
    }
}
