package tk.vexisu.vparkoursystem.objects;

/**
 * Created by Maciek on 2016-08-14.
 */
public class Score {

    private long time;
    private String nick;

    public Score(String nick, long time) {
        this.nick = nick;
        this.time = time;
    }

    public String getTime() {
        int ms1 = (int) time;
        final int secs = ms1 / 1000;
        ms1 %= 1000;
        return String.valueOf(secs) + "," + ms1;
    }

    public String getNick() {
        return nick;
    }

    public long getTimeInMilis() {
        return time;
    }
}
