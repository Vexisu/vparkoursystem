package tk.vexisu.vparkoursystem.objects;

/**
 * Created by Maciek on 2016-08-14.
 */
public class FailBlock {

    private int blockid;
    private byte blockdata;

    public FailBlock(int blockid, byte blockdata){
        this.blockid = blockid;
        this.blockdata = blockdata;
    }

    public int getBlockid() {
        return blockid;
    }

    public byte getBlockdata() {
        return blockdata;
    }
}
