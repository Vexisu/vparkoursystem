package tk.vexisu.vparkoursystem.objects;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import tk.vexisu.vparkoursystem.Main;

/**
 * Created by Maciek on 2016-08-14.
 */
public class Timer extends BukkitRunnable {

    private Player player;
    private long startTime;

    public Timer(final Player player) {
        this.player = player;
        this.startTime = System.currentTimeMillis();

        runTaskTimerAsynchronously((Plugin) Main.getInstance(), 1L, 1L);
    }

    // Timer

    public void run() {
        final int secondsPassed = (int) ((System.currentTimeMillis() - this.startTime) / 1000L);
        final float remainder = (int) ((System.currentTimeMillis() - this.startTime) % 1000L);
        final float tenthsPassed = remainder / 1000.0f;
        this.player.setLevel(secondsPassed);
        this.player.setExp(tenthsPassed);
    }

    // Getters and setters

    public long getTime() {
        return System.currentTimeMillis() - this.startTime;
    }
}
