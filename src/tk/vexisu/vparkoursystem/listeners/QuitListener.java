package tk.vexisu.vparkoursystem.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import tk.vexisu.vparkoursystem.objects.User;
import tk.vexisu.vparkoursystem.utils.UserUtil;

/**
 * Created by Maciek on 2016-08-14.
 */
public class QuitListener implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent e){
        Player p1 = e.getPlayer();
        User u1 = UserUtil.getUserByNick(p1.getName());
        u1.setActiveparkour(null);
        if(u1.getTimer() !=null){
            u1.getTimer().cancel();
            u1.setTimer(null);
        }
    }

}
