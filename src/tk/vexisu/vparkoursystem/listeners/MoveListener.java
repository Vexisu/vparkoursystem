package tk.vexisu.vparkoursystem.listeners;

import org.bukkit.Effect;
import org.bukkit.Sound;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import tk.vexisu.vparkoursystem.Main;
import tk.vexisu.vparkoursystem.objects.*;
import tk.vexisu.vparkoursystem.utils.ScoreboardUtil;
import tk.vexisu.vparkoursystem.utils.UserUtil;
import tk.vexisu.vparkoursystem.utils.Utils;

/**
 * Created by Maciek on 2016-08-14.
 */
public class MoveListener implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        Player p1 = e.getPlayer();
        User u1 = UserUtil.getUserByNick(p1.getName());
        if(u1.getActiveparkour() == null) {
            return;
        }
        Parkour pk1 = u1.getActiveparkour();

        for(FailBlock failBlock : pk1.getFailblocks()) {
            if(p1.getLocation().getBlock().getRelative(0, -1, 0).getTypeId() == failBlock.getBlockid() && p1.getLocation().getBlock().getRelative(0, -1, 0).getData() == failBlock.getBlockdata()) {
                fail(p1, u1, pk1);
                return;
            }
        }

        for (int i = -25; i != 1; i++) {
            if(p1.getLocation().getBlock().getRelative(0, i, 0).getTypeId() == 63 || p1.getLocation().getBlock().getRelative(0, i, 0).getTypeId() == 68) {
                Sign s = (Sign) p1.getLocation().getBlock().getRelative(0, i, 0).getState();
                if(s.getLine(0).equals("[VPS] start")){
                    if(u1.getTimer() == null){
                        start(p1, u1, pk1);
                        break;
                    }
                }
                if(s.getLine(0).equals("[VPS] end")){
                    if(u1.getTimer() != null){
                        end(p1, u1, pk1);
                        break;
                    }
                }
            }
        }
    }

    private static void fail(Player p1, User u1, Parkour pk1){
        if(u1.getTimer() != null){
            u1.getTimer().cancel();
            u1.setTimer(null);
        }
        Utils.updateParkour(p1);
        p1.teleport(pk1.getSpawn());
        p1.playSound(p1.getLocation(), Sound.ENDERMAN_TELEPORT, 1, 1);
        ScoreboardUtil.sendScoreBoard(p1, pk1);
    }

    private static void start(Player p1, User u1, Parkour pk1){
        Utils.updateParkour(p1);
        p1.playEffect(p1.getLocation(), Effect.POTION_BREAK, 1);
        u1.setTimer(new Timer(p1));
        Utils.sendMessage(p1, "Rozpoczales parkour " + pk1.getName() + "!");
        ScoreboardUtil.sendScoreBoard(p1, pk1);
    }

    private static void end(Player p1, User u1, Parkour pk1){
        Score s1 = pk1.getScoreByNick(p1.getName());
        long ltime = u1.getTimer().getTime();
        u1.getTimer().cancel();
        u1.setTimer(null);
        if(s1 == null || s1.getTimeInMilis() > ltime){
            pk1.getScores().remove(s1);
            pk1.getScores().add(new Score(p1.getName(), ltime));
            Main.getInstance().getConfig().set("Parkours." + pk1.getId() + ".Scores." + p1.getName(), ltime);
            Main.getInstance().saveConfig();
        }
        int ms1 = (int) ltime;
        final int secs = ms1 / 1000;
        ms1 %= 1000;
        String time = String.valueOf(secs) + "," + ms1;
        Utils.updateParkour(p1);
        p1.teleport(pk1.getSpawn());
        p1.playSound(p1.getLocation(), Sound.ENDERMAN_TELEPORT, 1, 1);
        Utils.sendMessage(p1, "Ukonczyles parkour " + pk1.getName() + " w czasie " + time + " sekund i zdobyles " + pk1.getExp() + " expa.");
        u1.setExp(u1.getExp() + pk1.getExp());
        Main.getInstance().getConfig().set("Users." + u1.getNick() + ".Exp", u1.getExp());
        Main.getInstance().saveConfig();
        Utils.levelUp(p1, u1);
        ScoreboardUtil.sendScoreBoard(p1, pk1);
    }
}
