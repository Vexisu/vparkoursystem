package tk.vexisu.vparkoursystem.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import tk.vexisu.vparkoursystem.Main;
import tk.vexisu.vparkoursystem.objects.User;
import tk.vexisu.vparkoursystem.utils.UserUtil;

/**
 * Created by Maciek on 2016-08-14.
 */
public class JoinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();

        if(Main.getInstance().getConfig().getConfigurationSection("Users." + p.getName()) == null) {
            String nickname = p.getName();
            Main.getInstance().getConfig().createSection("Users." + nickname);
            Main.getInstance().getConfig().set("Users." + nickname + ".Exp", 0);
            Main.getInstance().getConfig().set("Users." + nickname + ".Level", 1);
            Main.getInstance().saveConfig();

            UserUtil.getUsers().add(new User(nickname, 0, 1));
        }
    }
}
