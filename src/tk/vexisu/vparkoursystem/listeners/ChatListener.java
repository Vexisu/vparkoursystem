package tk.vexisu.vparkoursystem.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import tk.vexisu.vparkoursystem.objects.User;
import tk.vexisu.vparkoursystem.utils.UserUtil;

/**
 * Created by Maciek on 2016-08-20.
 */
public class ChatListener implements Listener {
    @EventHandler
    public void onChat(final AsyncPlayerChatEvent event) {
        final User user = UserUtil.getUserByNick(event.getPlayer().getName());
        if (user == null) {
            return;
        }
        String format = event.getFormat();
        format = format.replaceAll("\\{LVL\\}", new StringBuilder(String.valueOf(user.getLvl())).toString());
        event.setFormat(format);
    }
}
