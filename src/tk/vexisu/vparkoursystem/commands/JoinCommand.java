package tk.vexisu.vparkoursystem.commands;

import org.apache.commons.lang.StringUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.vexisu.vparkoursystem.objects.Parkour;
import tk.vexisu.vparkoursystem.objects.User;
import tk.vexisu.vparkoursystem.utils.ParkourUtil;
import tk.vexisu.vparkoursystem.utils.ScoreboardUtil;
import tk.vexisu.vparkoursystem.utils.UserUtil;
import tk.vexisu.vparkoursystem.utils.Utils;

/**
 * Created by Maciek on 2016-08-14.
 */
public class JoinCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(!(sender instanceof Player)) {
            Utils.sendMessage(sender, "Nie mozesz wykonac tej komendy z poziomu konsoli!");
            return true;
        }
        if(!sender.hasPermission("vpsystem.join")) {
            Utils.sendMessage(sender, "Nie masz uprawnien!");
            return true;
        }
        if(args.length == 1){
            if(!(args[0].length() <= 9 && StringUtils.isNumeric(args[0]))) {
                Utils.sendMessage(sender, "Bledne argumenty!");
                return true;
            }
            Parkour pk1 = ParkourUtil.getParkourByID(Integer.parseInt(args[0]));
            if(pk1 == null){
                Utils.sendMessage(sender, "Parkour z id " + args[0] + " nie istnieje!");
                return true;
            }
            Player p1 = (Player) sender;
            User u1 = UserUtil.getUserByNick(p1.getName());
            if(pk1.getRequiredlvl() > u1.getLvl()){
                Utils.sendMessage(sender, "Aby wejsc na ten parkour musisz miec " + pk1.getRequiredlvl() + " lvl.");
                return true;
            }
            if(u1.getTimer() != null){
                u1.getTimer().cancel();
                u1.setTimer(null);
            }
            u1.setActiveparkour(pk1);
            p1.teleport(pk1.getSpawn());
            Utils.updateParkour(p1);
            Utils.sendMessage(sender, "Teleportowales sie na parkour " + pk1.getName() + ".");
            ScoreboardUtil.sendScoreBoard(p1, pk1);
            return true;
        }
        Utils.sendMessage(sender, "Poprawne uzycie: /join <id>");
        return false;
    }
}
