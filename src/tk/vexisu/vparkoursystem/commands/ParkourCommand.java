package tk.vexisu.vparkoursystem.commands;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.vexisu.vparkoursystem.Main;
import tk.vexisu.vparkoursystem.objects.FailBlock;
import tk.vexisu.vparkoursystem.objects.Parkour;
import tk.vexisu.vparkoursystem.objects.Score;
import tk.vexisu.vparkoursystem.objects.User;
import tk.vexisu.vparkoursystem.utils.ParkourUtil;
import tk.vexisu.vparkoursystem.utils.UserUtil;
import tk.vexisu.vparkoursystem.utils.Utils;

import java.util.ArrayList;

/**
 * Created by Maciek on 2016-08-14.
 */
public class ParkourCommand implements CommandExecutor {

    private static Main instance = Main.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(!(sender instanceof Player)) {
            Utils.sendMessage(sender, "Nie mozesz wykonac tej komendy z poziomu konsoli!");
            return true;
        }
        if(!sender.hasPermission("vpsystem.admin")) {
            Utils.sendMessage(sender, "Nie masz uprawnien!");
            return true;
        }
        Player p = (Player) sender;
        if(args.length == 2) {
            if(!(args[1].length() <= 9 && StringUtils.isNumeric(args[1]))) {
                Utils.sendMessage(sender, "Id parkoura musi byc liczba!");
                return true;
            }
            if(args[0].equalsIgnoreCase("create")) {
                if(ParkourUtil.getParkourByID(Integer.parseInt(args[1])) != null) {
                    Utils.sendMessage(sender, "Parkour z id " + args[1] + " juz istnieje!");
                    return true;
                }
                int id = Integer.parseInt(args[1]);
                Location p1loc = p.getLocation();
                instance.getConfig().createSection("Parkours." + id);
                instance.getConfig().set("Parkours." + id + ".Name", "Parkour" + id);
                instance.getConfig().set("Parkours." + id + ".Exp", 0);
                instance.getConfig().set("Parkours." + id + ".RequiredLvl", 0);
                instance.getConfig().set("Parkours." + id + ".PkSpawn", Utils.deserializeLocation(p1loc));
                instance.getConfig().set("Parkours." + id + ".FailBlocks", "36");
                instance.getConfig().createSection("Parkours." + id + ".Scores");
                instance.saveConfig();
                ArrayList<FailBlock> failblocks = new ArrayList<>();
                failblocks.add(new FailBlock(36, (byte) 0));
                ParkourUtil.getParkours().add(new Parkour(id, "Parkour" + id, p1loc, 0, 0, new ArrayList<Score>(), failblocks));
                Utils.sendMessage(sender, "Parkour z id " + args[1] + " zostal utworzony!");
                return true;
            }

            if (args[0].equalsIgnoreCase("remove")){
                if (ParkourUtil.getParkourByID(Integer.parseInt(args[1])) == null) {
                    Utils.sendMessage(sender, "Parkour z id " + args[1] + " nie istnieje!");
                    return true;
                }
                Parkour pk1 = ParkourUtil.getParkourByID(Integer.parseInt(args[1]));
                for(Player p1 : Bukkit.getOnlinePlayers()){
                    User u1 = UserUtil.getUserByNick(p1.getName());
                    if(u1.getActiveparkour() == pk1){
                        Utils.sendMessage(sender, "Parkour z id " + pk1.getId() + " zostal usuniety!");
                        p1.chat("/lobby");
                    }
                }
                ParkourUtil.getParkourByID(Integer.parseInt(args[1])).removeParkour();
                Utils.sendMessage(sender, "Parkour z id " + args[1] + " zostal usuniety!");
                return true;
            }

            if (args[0].equalsIgnoreCase("clearscores")) {
                if (ParkourUtil.getParkourByID(Integer.parseInt(args[1])) == null) {
                    Utils.sendMessage(sender, "Parkour z id " + args[1] + " nie istnieje!");
                    return true;
                }

                Parkour pk1 = ParkourUtil.getParkourByID(Integer.parseInt(args[1]));
                pk1.getScores().clear();
                instance.getConfig().set("Parkours." + Integer.parseInt(args[1]) + ".Scores", null);
                instance.getConfig().createSection("Parkours." + Integer.parseInt(args[1]) + ".Scores");
                instance.saveConfig();
                Utils.sendMessage(sender, "Wyczyszczono czasy na parkourze z id " + args[1] + ".");
                return true;
            }

            if (args[0].equalsIgnoreCase("setspawn")) {
                if (ParkourUtil.getParkourByID(Integer.parseInt(args[1])) == null) {
                    Utils.sendMessage(sender, "Parkour z id " + args[1] + " nie istnieje!");
                    return true;
                }
                Parkour pk1 = ParkourUtil.getParkourByID(Integer.parseInt(args[1]));
                Location p1loc = p.getLocation();
                instance.getConfig().set("Parkours." + Integer.parseInt(args[1]) + ".PkSpawn", Utils.deserializeLocation(p1loc));
                pk1.setSpawn(p1loc);
                Utils.sendMessage(sender, "Spawn dla parkoura z id " + args[1] + " zostal ustawiony.");
                return true;
            }
        }
        if(args.length == 3) {

            if (args[0].equalsIgnoreCase("setplayerexp")) {
                if(!(StringUtils.isNumeric(args[2]) && args[2].length() <= 9)) {
                    Utils.sendMessage(sender, "Argument <exp> musi byc liczba!");
                    return true;
                }
                Player p1 = Bukkit.getPlayer(args[1]);
                if(p1 == null) {
                    Utils.sendMessage(sender, "Gracz " + args[1] + " nie jest online!");
                    return true;
                }
                User u1 = UserUtil.getUserByNick(p1.getName());
                int exp = Integer.parseInt(args[2]);
                u1.setLvl(0);
                u1.setExp(exp);
                Utils.silentLvlUp(u1);
                Utils.sendMessage(p1, "Twoje statystyki zostaly zmienione: " + u1.getLvl() + " lvl, " + u1.getExp() + " expa.");
                Utils.sendMessage(sender, "Statystyki gracza " + u1.getNick() + " zostaly zmienione: " + u1.getLvl() + " lvl, " + u1.getExp() + " expa.");
                return true;
            }


            if(!(args[1].length() <= 9 && StringUtils.isNumeric(args[1]))) {
                Utils.sendMessage(sender, "Id parkoura musi byc liczba!");
                return true;
            }
            if (args[0].equalsIgnoreCase("setfailblocks")) {
                if (ParkourUtil.getParkourByID(Integer.parseInt(args[1])) == null) {
                    Utils.sendMessage(sender, "Parkour z id " + args[1] + " nie istnieje!");
                    return true;
                }
                try{
                    String[] failBlocksArray = args[2].split(",");
                    ArrayList<FailBlock> failBlocksList = new ArrayList<>();
                    for (String failBlock : failBlocksArray){
                        String[] failBlockMeta = failBlock.split(":");
                        if(failBlockMeta.length == 1){
                            failBlocksList.add(new FailBlock(Integer.parseInt(failBlockMeta[0]), (byte) 0));
                        }
                        if(failBlockMeta.length == 2){
                            failBlocksList.add(new FailBlock(Integer.parseInt(failBlockMeta[0]), Byte.parseByte(failBlockMeta[1])));
                        }
                    }
                    Parkour pk1 = ParkourUtil.getParkourByID(Integer.parseInt(args[1]));
                    pk1.setFailblocks(failBlocksList);
                    instance.getConfig().set("Parkours." + Integer.parseInt(args[1]) + ".FailBlocks", args[2]);
                    instance.saveConfig();
                    Utils.sendMessage(sender, "Failblocki " + args[2] + " zostaly ustawione na parkourze z id " + args[1] + ".");
                }catch (Exception e){
                    Utils.sendMessage(sender, "Bledne argumenty blokow!");
                }
                return true;
            }

            if (args[0].equalsIgnoreCase("setname")) {
                if (ParkourUtil.getParkourByID(Integer.parseInt(args[1])) == null) {
                    Utils.sendMessage(sender, "Parkour z id " + args[1] + " nie istnieje!");
                    return true;
                }
                Parkour pk1 = ParkourUtil.getParkourByID(Integer.parseInt(args[1]));
                String name1= args[2];
                pk1.setName(name1);
                instance.getConfig().set("Parkours." + Integer.parseInt(args[1]) + ".Name", name1);
                instance.saveConfig();
                Utils.sendMessage(sender, "Nazwa parkoura z id " + args[1] + " zostala zmieniona na " + args[2] + ".");
                return true;
            }

            if (args[0].equalsIgnoreCase("setexp")) {
                if (ParkourUtil.getParkourByID(Integer.parseInt(args[1])) == null) {
                    Utils.sendMessage(sender, "Parkour z id " + args[1] + " nie istnieje!");
                    return true;
                }
                if(!(StringUtils.isNumeric(args[2]) && args[2].length() <= 9)) {
                    Utils.sendMessage(sender, "Argument <exp> musi byc liczba!");
                    return true;
                }
                Parkour pk1 = ParkourUtil.getParkourByID(Integer.parseInt(args[1]));
                int exp1 = Integer.parseInt(args[2]);
                pk1.setExp(exp1);
                instance.getConfig().set("Parkours." + Integer.parseInt(args[1]) + ".Exp", exp1);
                instance.saveConfig();
                Utils.sendMessage(sender, "Exp na parkourze z id " + args[1] + " zostal zmieniony na " + args[2] + " exp.");
                return true;
            }

            if (args[0].equalsIgnoreCase("setrequiredlvl")) {
                if (ParkourUtil.getParkourByID(Integer.parseInt(args[1])) == null) {
                    Utils.sendMessage(sender, "Parkour z id " + args[1] + " nie istnieje!");
                    return true;
                }
                if(!(StringUtils.isNumeric(args[2]) && args[2].length() <= 9)) {
                    Utils.sendMessage(sender, "Argument <lvl> musi byc liczba!");
                    return true;
                }
                Parkour pk1 = ParkourUtil.getParkourByID(Integer.parseInt(args[1]));
                int lvl1 = Integer.parseInt(args[2]);
                pk1.setRequiredlvl(lvl1);
                instance.getConfig().set("Parkours." + Integer.parseInt(args[1]) + ".RequiredLvl", lvl1);
                instance.saveConfig();
                Utils.sendMessage(sender, "Wymagany level dla parkoura z id " + args[1] + " zostal zmieniony na " + args[2] + " lvl.");
                return true;
            }

        }

        printHelp(sender);
        return false;
    }

    private static void printHelp(CommandSender sender) {
        sender.sendMessage(Utils.getColor("&9 >>>>>>>>>>   VParkourSystem   <<<<<<<<<<"));
        Utils.sendMessage(sender, "/parkour create <id>");
        Utils.sendMessage(sender, "/parkour remove <id>");
        Utils.sendMessage(sender, "/parkour setspawn <id>");
        Utils.sendMessage(sender, "/parkour setname <id> <name>");
        Utils.sendMessage(sender, "/parkour setexp <id> <exp>");
        Utils.sendMessage(sender, "/parkour setfailblocks <id> <block,block:data,etc...>");
        Utils.sendMessage(sender, "/parkour setrequiredlvl <id> <lvl>");
        Utils.sendMessage(sender, "/parkour clearscores <id>");
        Utils.sendMessage(sender, "/parkour setplayerexp <nick> <exp>");
        sender.sendMessage(Utils.getColor("&9 ----------------------------------"));
    }
}
