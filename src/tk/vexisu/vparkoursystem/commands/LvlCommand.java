package tk.vexisu.vparkoursystem.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.vexisu.vparkoursystem.objects.User;
import tk.vexisu.vparkoursystem.utils.UserUtil;
import tk.vexisu.vparkoursystem.utils.Utils;

/**
 * Created by Maciek on 2016-09-22.
 */
public class LvlCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lable, String[] args) {
        if(!(sender instanceof Player)) {
            Utils.sendMessage(sender, "Nie mozesz wykonac tej komendy z poziomu konsoli!");
            return true;
        }
        if(!sender.hasPermission("vpsystem.lvl")) {
            Utils.sendMessage(sender, "Nie masz uprawnien!");
            return true;
        }
        Player p1 = (Player) sender;
        if(args.length == 0){
            User u1 = UserUtil.getUserByNick(p1.getName());
            Utils.sendMessage(p1, "Aktualnie posiadasz " + u1.getLvl() + " lvl i " + u1.getExp() + " expa. Do nastepnego lvl'a brakuje Ci " + u1.getNeedExp() + " expa.");
            return true;
        }

        if(args.length == 1){
            User u1 = UserUtil.getUserByNick(args[0]);
            Utils.sendMessage(p1, "Gracz " + u1.getNick() + " aktualnie posiada " + u1.getLvl() + " lvl i " + u1.getExp() + " expa. Do nastepnego lvl'a brakuje mu " + u1.getNeedExp() + " expa.");
            return true;
        }
        return false;
    }
}
