package tk.vexisu.vparkoursystem.commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.vexisu.vparkoursystem.Main;
import tk.vexisu.vparkoursystem.extensions.ScoreboardBuilder;
import tk.vexisu.vparkoursystem.objects.User;
import tk.vexisu.vparkoursystem.utils.ConfigUtil;
import tk.vexisu.vparkoursystem.utils.UserUtil;
import tk.vexisu.vparkoursystem.utils.Utils;

/**
 * Created by Maciek on 2016-08-14.
 */
public class LobbyCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(!(sender instanceof Player)) {
            Utils.sendMessage(sender, "Nie mozesz wykonac tej komendy z poziomu konsoli!");
            return true;
        }
        Player p1 = (Player) sender;
        if(args.length == 1){
            if(sender.hasPermission("vpsystem.setlobby") && args[0].equalsIgnoreCase("set")){
                Location l1 = p1.getLocation();
                String lobby = Utils.deserializeLocation(l1);
                Main.getInstance().getConfig().set("Lobby", lobby);
                Main.getInstance().saveConfig();
                ConfigUtil.setLobby(l1);
                Utils.sendMessage(sender, "Zmieniles lokalizacje lobby.");
                return true;
            }
        }
        if(!sender.hasPermission("vpsystem.lobby")) {
            Utils.sendMessage(sender, "Nie masz uprawnien!");
            return true;
        }
        if(ConfigUtil.getLobby() == null){
            Utils.sendMessage(sender, "Lobby nie zostalo ustawione!");
            return true;
        }
        User u1 = UserUtil.getUserByNick(p1.getName());
        if(u1.getActiveparkour() != null){
            u1.setActiveparkour(null);
        }
        if(u1.getTimer() !=null){
            u1.getTimer().cancel();
            u1.setTimer(null);
        }
        p1.teleport(ConfigUtil.getLobby());
        Utils.updateParkour(p1);
        ScoreboardBuilder.destroy(p1);
        Utils.sendMessage(sender, "Teleportowales sie na lobby.");
        return false;
    }
}
